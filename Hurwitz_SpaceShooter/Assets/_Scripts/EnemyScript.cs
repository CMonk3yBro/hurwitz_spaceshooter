﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    public float speed;
    private Rigidbody rb;

    public int scoreValue;
    public int minionsLeftValue;

    public GameObject explosion;
    public GameObject playerExplosion;
    
    private GameController gameController;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.velocity = Vector3.forward * speed;

        GameObject gameControllerObject = GameObject.FindWithTag("GameController");

        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Boundary") || other.CompareTag("Enemy") || other.CompareTag("Boss") || other.CompareTag("Ant"))
        {
            return;
        }

        if (explosion != null)
        {
            Instantiate(explosion, transform.position, transform.rotation);
        }

        if (other.tag == "PlayerBlast")
        {
            speed += 1;
            rb.velocity = Vector3.forward * speed;
            Destroy(other.gameObject);
            gameController.AddScore(scoreValue);
        }

        if (speed >= 0)
        {
            Destroy(gameObject);
            gameController.AddMinionsLeftScore(minionsLeftValue);
        }

        if (other.tag == "Player")
        {
            Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
            //Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }
}
