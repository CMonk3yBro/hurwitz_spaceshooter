﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public bool isCreated = true;

    public GameObject boss;
    public GameObject[] hazards;
    public Vector3 spawnValues;
    public int hazardCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;

    public Text scoreText;
    public Text restartText;
    public Text gameOverText;
    public Text playerHealthText;
    public Text minionsLeftText;

    public GameObject restarttextbox;

    private bool gameOver;
    private bool restart;
    private int score;
    private int playerHealthScore;
    private int minionsLeftScore;

    // Start is called before the first frame update
    void Start()
    {
        gameOver = false;
        restart = false;
        restartText.text = "";
        gameOverText.text = "";

        score = 0;
        playerHealthScore = 5;
        minionsLeftScore = 20;
        UpdateScore();
        StartCoroutine (SpawnWaves ());
    }

    void Update()
    {
        if (restart)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }

        if (minionsLeftScore <= 0)
        {
            minionsLeftText.text = "Ant Queen!";

            if (isCreated == true)
            {
                Instantiate(boss, new Vector3(0, 0, 25), Quaternion.identity);
                //Debug.Log("Boss spawned!");
                isCreated = false;
                minionsLeftScore = 20;
                isCreated = true;
            }
        }

        if (playerHealthScore <= 0)
        {
            Destroy(GameObject.Find("Player"));
            GameOver();
        }
    }

    IEnumerator SpawnWaves ()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            for (int i = 0; i < hazardCount; i++)
            {
                GameObject hazard = hazards[Random.Range(0, hazards.Length)];
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(hazard, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);

            if (gameOver)
            {
                //Instantiate(restarttextbox, new Vector3(4.81f, 0, -5.65f), Quaternion.identity);
                restartText.text = "Press 'R' for Restart";
                restart = true;
                break;
            }
        }
    }

    public void AddScore(int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }
    public void AddPlayerHealthScore(int newPlayerHealthScoreValue)
    {
        playerHealthScore -= 1;
        UpdateScore();
    }
    public void AddMinionsLeftScore(int newMinionsLeftScoreValue)
    {
        minionsLeftScore -= newMinionsLeftScoreValue;
        UpdateScore();
    }

    void UpdateScore()
    {
        scoreText.text = "Score: " + score;
        playerHealthText.text = "Player Health: " + playerHealthScore;
        minionsLeftText.text = "Minions Left: " + minionsLeftScore;
    }

    public void GameOver()
    {
        gameOverText.text = "Game Over!";
        gameOver = true;
    }
}